const path = require('path')

const resolve = (p) => path.join(process.cwd(), p)

module.exports = {
    webpackFinal(config) {
        config.resolve.alias = {
            ...config.resolve.alias,
            '@emotion/styled': resolve('node_modules/@emotion/styled'),
        }
        return config
    },
    addons: ['@storybook/addon-knobs', '@storybook/addon-actions'],
    stories: ['../src/stories/**/*.stories.tsx'],
}
