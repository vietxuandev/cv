import React, { FC, memo, useMemo } from 'react'
import { Card, Flex, IconButton, Image, Text, useColorMode } from 'theme-ui'
import ReactEmoji from 'react-emoji'
import Linkify from 'react-linkify'
import dayjs from 'dayjs'
import { FileIcon } from 'react-file-icon'
import ReplyIcon from '../assets/icons/reply.svg'
import DownloadIcon from '../assets/icons/download.svg'

interface File {
    _id: string
    filename: string
    mimetype: string
    src: string
}

export type Status = 'SEND' | 'SENT' | 'SEEN' | 'REVOKE' | 'REMOVE'
export interface IMessage {
    _id: string
    content: string
    createdAt: string
    sender: { _id: string; firstName: string; lastName: string }
    message?: IMessage
    files?: File[]
    status: Status
}

interface MessageProps {
    partner?: boolean
    message: IMessage
    nextMessage: IMessage
    prevMessage: IMessage
    userId: string
    onReply: () => void
}

const Message: FC<MessageProps> = ({
    message,
    partner,
    nextMessage,
    prevMessage,
    userId,
    onReply,
}) => {
    const { createdAt, sender, message: replyMessage, files } = message
    const time = useMemo(() => {
        if (dayjs().isSame(dayjs(createdAt), 'year')) {
            if (dayjs().isSame(dayjs(createdAt), 'month')) {
                if (dayjs().isSame(dayjs(createdAt), 'week')) {
                    if (dayjs().isSame(dayjs(createdAt), 'day')) {
                        return dayjs(createdAt).format('HH:ss')
                    }
                    return dayjs(createdAt).format('dd HH:ss')
                }
                return dayjs(createdAt).format('DD HH:ss')
            }
            return dayjs(createdAt).format('DD/MM HH:ss')
        }
        return dayjs(createdAt).format('DD/MM/YYYY HH:ss')
    }, [createdAt])
    const isNew = useMemo(
        () =>
            new Date(createdAt).getTime() -
                new Date(prevMessage?.createdAt).getTime() >
            300000,
        [createdAt, prevMessage]
    )
    const isOld = useMemo(
        () =>
            new Date(nextMessage?.createdAt).getTime() -
                new Date(createdAt).getTime() >
            300000,
        [createdAt, nextMessage]
    )
    const isFirst = useMemo(() => sender?._id !== prevMessage?.sender._id, [
        prevMessage,
        sender,
    ])
    const isLast = useMemo(() => sender?._id !== nextMessage?.sender._id, [
        nextMessage,
        sender,
    ])
    const detail = useMemo(() => {
        if (replyMessage) {
            if (sender._id === userId) {
                if (sender._id === userId) {
                    return 'Bạn đã trả lời chính bạn'
                }
                return `${sender.firstName} đã trả lời bạn`
            }
            if (sender._id === userId) {
                return `Bạn đã trả lời ${sender.firstName}`
            }
            return `${sender.firstName} đã trả lời ${sender.firstName}`
        }
        return ''
    }, [replyMessage, sender, userId])
    const [theme] = useColorMode()
    return (
        <Flex sx={{ flexDirection: 'column' }}>
            {isNew && (
                <Flex sx={{ justifyContent: 'center' }}>
                    <Text
                        color="textSecondary"
                        sx={{
                            fontSize: 0,
                            textAlign: 'center',
                        }}
                    >
                        {time}
                    </Text>
                </Flex>
            )}
            <Flex
                {...(partner
                    ? { mr: 'auto' }
                    : {
                          ml: 'auto',
                      })}
                sx={{
                    maxWidth: '80%',
                    width: '100%',
                    flexDirection: 'column',
                }}
            >
                {detail && (
                    <>
                        <Flex
                            color="primary"
                            sx={{
                                alignItems: 'center',
                                justifyContent: partner
                                    ? 'flex-start'
                                    : 'flex-end',
                            }}
                        >
                            <ReplyIcon
                                width={10}
                                height={10}
                                fill="currentColor"
                            />
                            <Text
                                ml={1}
                                color="textSecondary"
                                sx={{
                                    fontSize: 0,
                                }}
                            >
                                {detail}
                            </Text>
                        </Flex>
                        <Flex
                            {...(partner
                                ? { mr: 'auto' }
                                : {
                                      ml: 'auto',
                                  })}
                            mb={-3}
                            {...(partner
                                ? { bg: 'background' }
                                : {
                                      bg: 'message',
                                  })}
                            pt={2}
                            pb="18px"
                            px={3}
                            sx={{
                                opacity: 0.6,
                                borderRadius: 1,
                                borderStyle: 'solid',
                                borderWidth: 1,
                                borderColor: 'message',
                                overflowWrap: 'break-word',
                                cursor: 'pointer',
                            }}
                        >
                            <Text
                                color={
                                    theme === 'dark' && !partner
                                        ? 'black'
                                        : 'text'
                                }
                                sx={{ fontSize: 0 }}
                                title={time}
                            >
                                <Linkify target="_blank">
                                    {ReactEmoji.emojify(replyMessage.content)}
                                </Linkify>
                            </Text>
                        </Flex>
                    </>
                )}
                <Flex
                    sx={{
                        ...{
                            alignItems: 'center',
                            ':hover': {
                                button: {
                                    opacity: 1,
                                },
                            },
                        },
                        ...(!partner && { flexDirection: 'row-reverse' }),
                    }}
                >
                    <Flex
                        mb="2px"
                        {...(partner
                            ? { bg: 'background' }
                            : {
                                  bg: 'message',
                              })}
                        py={2}
                        px={3}
                        sx={{
                            ...{
                                zIndex: 1,
                                borderRadius: 0,
                                borderStyle: 'solid',
                                borderWidth: 1,
                                borderColor: 'message',
                                overflowWrap: 'break-word',
                                flexDirection: 'column',
                            },
                            ...(partner
                                ? {
                                      ...{
                                          borderTopRightRadius: 1,
                                          borderBottomRightRadius: 1,
                                      },
                                      ...((isFirst || isNew) && {
                                          borderTopLeftRadius: 1,
                                      }),
                                      ...((isOld || isLast) && {
                                          borderBottomLeftRadius: 1,
                                      }),
                                  }
                                : {
                                      ...{
                                          borderTopLeftRadius: 1,
                                          borderBottomLeftRadius: 1,
                                      },
                                      ...((isFirst || isNew) && {
                                          borderTopRightRadius: 1,
                                      }),
                                      ...((isOld || isLast) && {
                                          borderBottomRightRadius: 1,
                                      }),
                                  }),
                        }}
                    >
                        <Text
                            color={
                                theme === 'dark' && !partner ? 'black' : 'text'
                            }
                            sx={{ fontSize: 2 }}
                            title={time}
                        >
                            <Linkify target="_blank">
                                {ReactEmoji.emojify(message.content)}
                            </Linkify>
                        </Text>
                    </Flex>
                    <IconButton
                        color="textSecondary"
                        sx={{
                            opacity: 0,
                            zIndex: 1,
                            transition: '0.3s',
                            cursor: 'pointer',
                            flexShrink: 0,
                            ':hover': {
                                color: 'primary',
                            },
                        }}
                        onClick={onReply}
                    >
                        <ReplyIcon fill="currentColor" />
                    </IconButton>
                </Flex>
                {files &&
                    files.map((file) => (
                        <Flex
                            key={file._id}
                            px={1}
                            mb={1}
                            sx={{
                                ...{
                                    alignItems: 'center',
                                    ':hover': {
                                        button: { opacity: 1 },
                                    },
                                },
                                ...(!partner && {
                                    flexDirection: 'row-reverse',
                                }),
                            }}
                        >
                            <Card
                                sx={{
                                    maxWidth: 256,
                                    width: '100%',
                                }}
                            >
                                {file.mimetype.includes('image') ? (
                                    <Image
                                        src={`http://localhost:4000/${file.src}`}
                                        sx={{
                                            width: '100%',
                                        }}
                                    />
                                ) : (
                                    <Flex>
                                        <Flex
                                            mr={2}
                                            sx={{
                                                flexShrink: 0,
                                                svg: {
                                                    width: 40,
                                                    height: 40,
                                                },
                                                color: 'primary',
                                            }}
                                        >
                                            <FileIcon
                                                extension={
                                                    file.filename
                                                        .split('.')
                                                        .slice(-1)[0]
                                                }
                                                type="document"
                                                labelColor="currentColor"
                                            />
                                        </Flex>
                                        <Text
                                            sx={{ fontSize: 1, color: 'text' }}
                                        >
                                            {file.filename}
                                        </Text>
                                    </Flex>
                                )}
                            </Card>
                            <a
                                href={`http://localhost:4000/${file.src}`}
                                download={file.filename}
                                target="_blank"
                                rel="noreferrer"
                            >
                                <IconButton
                                    mx={2}
                                    sx={{
                                        cursor: 'pointer',
                                        opacity: 0,
                                        transition: '0.3s',
                                        color: 'textSecondary',
                                        width: 22,
                                        height: 22,
                                        ':hover': {
                                            color: 'primary',
                                        },
                                    }}
                                >
                                    <DownloadIcon fill="currentColor" />
                                </IconButton>
                            </a>
                        </Flex>
                    ))}
                {message.status === 'SEEN' && !partner && !nextMessage && (
                    <Text
                        px={2}
                        color="textSecondary"
                        sx={{ fontSize: 0, textAlign: 'right' }}
                    >
                        Đã xem
                    </Text>
                )}
            </Flex>
        </Flex>
    )
}

export default memo(Message)
