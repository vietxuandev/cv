import React from 'react'

import { Story } from '@storybook/react'
import { boolean, withKnobs } from '@storybook/addon-knobs'

import Menu, { MenuProps } from '../components/Menu'

export default {
    title: 'Menu',
    component: Menu,
    decorators: [withKnobs],
    argTypes: { onChange: { action: 'onChange' } },
}

const Template: Story<MenuProps> = (args) => (
    <Menu open={boolean('open', false)} {...args} />
)

export const DefaultMenu = Template.bind({})
DefaultMenu.args = {}
