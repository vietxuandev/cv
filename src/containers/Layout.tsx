import React, { FC, memo, PropsWithChildren } from 'react'
import { Box } from 'theme-ui'
import Header from '../components/Header'

const Layout: FC<PropsWithChildren<Record<string, unknown>>> = ({
    children,
}) => {
    return (
        <Box>
            <Header />
            <Box mx="auto" p={20} sx={{ maxWidth: 900 }}>
                {children}
            </Box>
        </Box>
    )
}

export default memo(Layout)
