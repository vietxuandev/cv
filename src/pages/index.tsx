import { Flex, Input, Text } from 'theme-ui'
import React, { FC, memo, useCallback } from 'react'
import { gql, useMutation, useQuery } from '@apollo/client'
import { AVATAR_UPLOAD } from '../gql'

export const CURRENT_USER = gql`
    query GetUser {
        currentUser {
            _id
            email
            firstName
            lastName
            phoneNumber
        }
    }
`

export interface CurrentUser {
    _id: string
    email: string
    firstName: string
    lastName: string
    phoneNumber: string
}

const Home: FC = () => {
    const { data } = useQuery<{ currentUser: CurrentUser }>(CURRENT_USER, {
        errorPolicy: 'all',
        fetchPolicy: 'network-only',
    })

    const [mutate] = useMutation(AVATAR_UPLOAD)
    const onChange = useCallback(
        ({
            target: {
                validity,
                files: [file],
            },
        }) => {
            console.log(file)
            if (validity.valid) mutate({ variables: { file } })
        },
        [mutate]
    )
    return (
        <Flex sx={{ flexDirection: 'column' }}>
            <Text>{data?.currentUser?.email}</Text>
            <Text>{data?.currentUser?.firstName}</Text>
            <Text>{data?.currentUser?.lastName}</Text>
            <Text>{data?.currentUser?.phoneNumber}</Text>
            <Input type="file" required onChange={onChange} />
        </Flex>
    )
}

export default memo(Home)
