import { useMutation, useQuery, useSubscription } from '@apollo/client'
import React, {
    FC,
    useCallback,
    useEffect,
    useRef,
    useState,
    useMemo,
    memo,
} from 'react'
import Head from 'next/head'
import { Box, Flex, Input, Text, Spinner, Close, IconButton } from 'theme-ui'
import { keyframes } from '@emotion/react'
import InfiniteScroll from 'react-infinite-scroll-component'
import ReactEmoji from 'react-emoji'
import Linkify from 'react-linkify'
import { v4 as uuid } from 'uuid'
import { SubmitHandler, useForm } from 'react-hook-form'
import { GetServerSideProps } from 'next'
import { CurrentUser, CURRENT_USER } from '..'
import Typing from '../../components/Typing'
import {
    CREATE_MESSAGE,
    MESSAGES,
    NEW_MESSAGE,
    NEW_TYPING,
    TYPING,
    LAST_SEEN,
    NEW_LAST_SEEN,
} from '../../gql'
import Message, { IMessage, Status } from '../../components/Message'
import AttachmentIcon from '../../assets/icons/attachment.svg'
import ImageIcon from '../../assets/icons/image.svg'
import SendIcon from '../../assets/icons/send.svg'

const LIMIT_MESSAGE = 20

const jump = keyframes`
        0%   { transform: translateY(0); }
        50%  { transform: translateY(-3px); }
        100% { transform: translateY(0); }
`
interface IFormInput {
    content: string
}

const HaveNewMessage: FC<{ onClick: () => void }> = memo(({ onClick }) => (
    <Flex
        px={2}
        bg="background"
        sx={{
            zIndex: 2,
            height: 30,
            boxShadow: 'small',
            position: 'absolute',
            bottom: 42,
            right: 0,
            borderRadius: 1,
            cursor: 'pointer',
            animation: `${jump} 1s ease infinite`,
            alignItems: 'center',
        }}
    >
        <Text
            onClick={onClick}
            color="text"
            sx={{
                fontSize: 1,
            }}
        >
            Có tin nhắn mới
        </Text>
    </Flex>
))

interface ConversationPageProps {
    conversationId: string | string[]
}

export const getServerSideProps: GetServerSideProps<ConversationPageProps> = async ({
    query,
}) => {
    const { conversationId } = query
    return {
        props: { conversationId },
    }
}

const ConversationPage: FC<ConversationPageProps> = ({ conversationId }) => {
    const [hasMore, setHasMore] = useState<boolean>(true)
    const scrollRef = useRef<HTMLDivElement>(null)
    const messagesEndRef = useRef<HTMLDivElement>(null)
    const [newMessage, setNewMessage] = useState<IMessage | null>(null)
    const [replyMessage, setReplyMessage] = useState<IMessage | null>(null)
    const [showScrollToBottom, setShowScrollToBottom] = useState<boolean>(false)
    const {
        subscribeToMore,
        data: dataMessages,
        fetchMore,
        updateQuery,
    } = useQuery(MESSAGES, {
        errorPolicy: 'all',
        fetchPolicy: 'cache-and-network',
        variables: {
            conversationId,
            offset: 0,
            limit: LIMIT_MESSAGE,
        },
    })
    const { data: dataUser } = useQuery<{
        currentUser: CurrentUser
    }>(CURRENT_USER, {
        errorPolicy: 'all',
        fetchPolicy: 'network-only',
    })
    const { data: dataTyping } = useSubscription(NEW_TYPING, {
        variables: { conversation: conversationId },
    })
    const { data: dataLastSeen } = useSubscription(NEW_LAST_SEEN, {
        variables: { conversation: conversationId },
    })
    const [onTyping] = useMutation(TYPING, {
        errorPolicy: 'all',
    })
    const [onLastSeen] = useMutation(LAST_SEEN, {
        errorPolicy: 'all',
    })
    const [createMessage, { data: dataMessage }] = useMutation(CREATE_MESSAGE, {
        errorPolicy: 'all',
    })
    const userId = useMemo<string>(() => dataUser?.currentUser?._id ?? '', [
        dataUser?.currentUser?._id,
    ])
    const messages = useMemo<IMessage[]>(() => dataMessages?.messages ?? [], [
        dataMessages?.messages,
    ])
    const message = useMemo<IMessage>(
        () => dataMessage?.createMessage ?? null,
        [dataMessage?.createMessage]
    )
    const {
        register,
        handleSubmit,
        reset,
        setFocus,
        formState: { isDirty },
    } = useForm<IFormInput>({ defaultValues: { content: '' } })

    const fetchMoreMessage = useCallback(() => {
        fetchMore({
            variables: {
                conversationId,
                offset: (messages ?? []).length,
                limit: LIMIT_MESSAGE,
            },
        }).then((fetchMoreResult) => {
            const newEntries = fetchMoreResult.data.messages
            if ((newEntries ?? []).length < LIMIT_MESSAGE) {
                setHasMore(false)
            }
        })
    }, [fetchMore, conversationId, messages])
    const subscribeToNewMessage = useCallback(() => {
        if (subscribeToMore) {
            subscribeToMore({
                document: NEW_MESSAGE,
                variables: { conversation: conversationId },
                updateQuery: (prev, { subscriptionData }) => {
                    if (!subscriptionData.data) return prev
                    const newFeedItem = subscriptionData.data.newMessage
                    setNewMessage(newFeedItem)
                    return {
                        ...prev,
                        messages: [newFeedItem, ...prev.messages],
                    }
                },
            })
        }
    }, [subscribeToMore, conversationId])
    const scrollToBottom = useCallback(() => {
        messagesEndRef.current?.scrollIntoView({ behavior: 'smooth' })
        if (newMessage) {
            setNewMessage(null)
        }
    }, [newMessage])
    const [listFile, setFiles] = useState<Array<File>>()
    const onChange = useCallback(({ target: { validity, files } }) => {
        if (validity.valid) {
            setFiles(Array.from(files))
        }
    }, [])
    const filesRef = useRef<HTMLInputElement>(null)
    const onSubmit = useCallback<SubmitHandler<IFormInput>>(
        ({ content }) => {
            if (dataUser?.currentUser) {
                const _id = uuid()
                const createMessageData = {
                    _id,
                    createdAt: new Date().toISOString(),
                    message: replyMessage || null,
                    content,
                    status: 'SEND' as Status,
                    sender: dataUser?.currentUser,
                }
                setNewMessage(createMessageData)
                updateQuery((prev) => {
                    return {
                        ...prev,
                        messages: [createMessageData, ...prev.messages],
                    }
                })
                createMessage({
                    variables: {
                        ...{
                            conversation: conversationId,
                            content,
                            _id,
                            files: listFile,
                        },
                        ...(replyMessage && { message: replyMessage._id }),
                    },
                })
                reset()
                setReplyMessage(null)
                filesRef.current.value = null
            }
        },
        [
            createMessage,
            conversationId,
            reset,
            replyMessage,
            dataUser?.currentUser,
            updateQuery,
            listFile,
        ]
    )
    const onSeen = useCallback(() => {
        if (messages) {
            const lastMessage = messages.find(
                (item) => item.sender._id !== userId && item.status !== 'SEEN'
            )
            if (lastMessage) {
                onLastSeen({
                    variables: {
                        conversationId,
                    },
                })
            }
        }
    }, [conversationId, onLastSeen, messages, userId])
    const onScroll = useCallback(() => {
        if (scrollRef.current.scrollTop === 0) {
            setShowScrollToBottom(false)
        }
    }, [])
    // +2 render
    useEffect(() => {
        if (newMessage && messages[0]?._id === newMessage._id) {
            if (
                newMessage.sender._id !== userId &&
                scrollRef.current.scrollTop !== 0
            )
                setShowScrollToBottom(true)
            else scrollToBottom()
        } else if (!showScrollToBottom) setShowScrollToBottom(false)
    }, [newMessage, userId, scrollToBottom, messages, showScrollToBottom])
    // +2 render
    useEffect(() => {
        onTyping({ variables: { conversationId, typing: isDirty } })
    }, [isDirty, onTyping, conversationId])
    useEffect(() => subscribeToNewMessage(), [subscribeToNewMessage])
    useEffect(() => {
        setFocus('content')
    }, [setFocus])
    useEffect(() => {
        if (dataLastSeen && userId) {
            updateQuery((prev) => {
                const newData = prev.messages.map((item) =>
                    item.sender._id === userId && item.status !== 'SEEN'
                        ? { ...item, status: 'SEEN' }
                        : item
                )
                return {
                    messages: newData,
                }
            })
        }
    }, [dataLastSeen, updateQuery, userId])
    useEffect(() => {
        if (userId && message) {
            updateQuery((prev) => {
                const newData = prev.messages.map((item) =>
                    item._id === message._id ? message : item
                )
                return {
                    messages: newData,
                }
            })
        }
    }, [message, userId, updateQuery])
    return (
        <>
            <Head>{newMessage && <title>Có tin nhắn mới</title>}</Head>
            <Flex
                sx={{
                    flexDirection: 'column',
                }}
            >
                <Input
                    ref={filesRef}
                    type="file"
                    required
                    onChange={onChange}
                    multiple
                />
                <Flex
                    ref={scrollRef}
                    id="scrollableDiv"
                    sx={{
                        height: 300,
                        overflow: 'auto',
                        flexDirection: 'column-reverse',
                    }}
                    onScroll={onScroll}
                >
                    <InfiniteScroll
                        dataLength={(messages ?? []).length}
                        next={fetchMoreMessage}
                        style={{
                            display: 'flex',
                            flexDirection: 'column-reverse',
                        }}
                        inverse
                        hasMore={hasMore}
                        loader={
                            <Flex sx={{ justifyContent: 'center' }}>
                                <Spinner size={20} />
                            </Flex>
                        }
                        scrollableTarget="scrollableDiv"
                        endMessage={
                            <Flex sx={{ justifyContent: 'center' }}>
                                <Text
                                    color="textSecondary"
                                    sx={{ fontSize: 1 }}
                                >
                                    Bạn đã xem toàn bộ tin nhắn
                                </Text>
                            </Flex>
                        }
                    >
                        <Box ref={messagesEndRef} />
                        {dataTyping?.newTyping?.typing && <Typing />}
                        {messages?.map((item, index) => (
                            <Message
                                key={item._id}
                                partner={userId !== item?.sender?._id}
                                message={item}
                                prevMessage={messages[index + 1]}
                                nextMessage={messages[index - 1]}
                                onReply={() => {
                                    setReplyMessage(item)
                                    setFocus('content')
                                }}
                                userId={userId}
                            />
                        ))}
                    </InfiniteScroll>
                </Flex>
                {showScrollToBottom && (
                    <HaveNewMessage onClick={scrollToBottom} />
                )}
                <Flex
                    as="form"
                    onSubmit={handleSubmit(onSubmit)}
                    sx={{
                        borderRadius: 1,
                        borderStyle: 'solid',
                        borderWidth: 1,
                        borderColor: 'message',
                        flexDirection: 'column',
                    }}
                >
                    <Flex sx={{ flexDirection: 'row', alignItems: 'center' }}>
                        <IconButton
                            ml={2}
                            sx={{
                                flexShrink: 0,
                                cursor: 'pointer',
                                transition: '0.3s',
                                color: 'textSecondary',
                                ':hover': {
                                    color: 'primary',
                                },
                            }}
                        >
                            <ImageIcon
                                width={20}
                                height={20}
                                fill="currentColor"
                            />
                        </IconButton>
                        <IconButton
                            sx={{
                                flexShrink: 0,
                                cursor: 'pointer',
                                transition: '0.3s',
                                color: 'textSecondary',
                                ':hover': {
                                    color: 'primary',
                                },
                            }}
                        >
                            <AttachmentIcon
                                width={20}
                                height={20}
                                fill="currentColor"
                            />
                        </IconButton>
                        <Input
                            {...register('content', { required: true })}
                            px={2}
                            sx={{
                                outline: 0,
                                border: 0,
                            }}
                            placeholder="Nhắn tin..."
                            autoComplete="off"
                            onFocus={onSeen}
                        />
                        <IconButton
                            mr={2}
                            type="submit"
                            color="textSecondary"
                            sx={{
                                flexShrink: 0,
                                cursor: 'pointer',
                                transition: '0.3s',
                                ':hover': { color: 'primary' },
                            }}
                        >
                            <SendIcon fill="currentColor" />
                        </IconButton>
                    </Flex>
                    {replyMessage && (
                        <Flex sx={{ flexDirection: 'row' }}>
                            <Flex
                                px={3}
                                py={2}
                                sx={{
                                    top: -19,
                                    left: 0,
                                    right: 0,
                                    flexDirection: 'column',
                                }}
                            >
                                <Text
                                    color="textSecondary"
                                    sx={{ fontSize: 0 }}
                                >
                                    Đang trả lời{' '}
                                    {userId === replyMessage.sender._id
                                        ? 'chính bạn'
                                        : `${replyMessage.sender.firstName}`}
                                </Text>
                                <Text
                                    color="text"
                                    sx={{
                                        fontSize: 2,
                                        textOverflow: 'ellipsis',
                                        overflow: 'hidden',
                                        whiteSpace: 'nowrap',
                                    }}
                                >
                                    <Linkify target="_blank">
                                        {ReactEmoji.emojify(
                                            replyMessage.content
                                        )}
                                    </Linkify>
                                </Text>
                            </Flex>
                            <Close
                                onClick={() => setReplyMessage(null)}
                                ml="auto"
                                color="textSecondary"
                                sx={{
                                    svg: {
                                        width: 15,
                                        height: 15,
                                    },
                                    cursor: 'pointer',
                                    flexShrink: 0,
                                    ':hover': {
                                        color: 'primary',
                                    },
                                    transition: '0.3s',
                                }}
                            />
                        </Flex>
                    )}
                </Flex>
            </Flex>
        </>
    )
}

export default memo(ConversationPage)
