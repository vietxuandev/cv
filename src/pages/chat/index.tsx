import { gql, useQuery } from '@apollo/client'
import { Heading, Text, Flex } from 'theme-ui'
import Link from 'next/link'
import React, { FC, memo } from 'react'

export const CONVERSATIONS = gql`
    query GetConversations {
        conversations {
            _id
            name
        }
    }
`

interface Conversations {
    _id: string
    name: string
}

const ChatPage: FC = () => {
    const { data } = useQuery<{
        conversations: Conversations[]
    }>(CONVERSATIONS, {
        errorPolicy: 'all',
    })
    return (
        <>
            <Heading as="h1">Cuộc trò chuyện</Heading>
            <Flex sx={{ flexDirection: 'column' }}>
                {data?.conversations?.map((conversation) => (
                    <Link
                        key={conversation._id}
                        href={`chat/${conversation._id}`}
                        passHref
                    >
                        <Text as="a">{conversation.name}</Text>
                    </Link>
                ))}
            </Flex>
        </>
    )
}

export default memo(ChatPage)
