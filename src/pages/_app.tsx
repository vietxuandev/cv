import '../styles/globals.css'

import React, { FC } from 'react'
import { AppProps } from 'next/app'
import { ThemeProvider } from 'theme-ui'
import Router from 'next/router'
import NProgress from 'nprogress'
import {
    ApolloClient,
    InMemoryCache,
    ApolloProvider,
    split,
} from '@apollo/client'
import { setContext } from '@apollo/client/link/context'
import { WebSocketLink } from '@apollo/client/link/ws'
import createUploadLink from 'apollo-upload-client/public/createUploadLink.js'
import {
    getMainDefinition,
    offsetLimitPagination,
} from '@apollo/client/utilities'
import { theme } from '../styles/theme'

import 'nprogress/nprogress.css'
import Layout from '../containers/Layout'

NProgress.configure({ showSpinner: false })
Router.events.on('routeChangeStart', () => NProgress.start())
Router.events.on('routeChangeComplete', () => {
    NProgress.done()
    window.scrollTo(0, 0)
})
Router.events.on('routeChangeError', () => NProgress.done())
const uploadLink = createUploadLink({ uri: 'http://localhost:4000/graphql' })

const getToken: () => string = () => {
    const token = localStorage.getItem('auth-token')
    return token ? `Bearer ${JSON.parse(token)}` : ''
}

export const wsLink = process.browser
    ? new WebSocketLink({
          uri: 'ws://localhost:4000/graphql',
          options: {
              lazy: true,
              reconnect: true,
              connectionParams: () => ({
                  Authorization: getToken(),
              }),
          },
      })
    : null

const authLink = setContext((_, { headers }) => {
    return {
        headers: {
            ...headers,
            Authorization: getToken(),
        },
    }
})

const splitLink = process.browser
    ? split(
          ({ query }) => {
              const definition = getMainDefinition(query)
              return (
                  definition.kind === 'OperationDefinition' &&
                  definition.operation === 'subscription'
              )
          },
          wsLink,
          authLink.concat(uploadLink)
      )
    : authLink.concat(uploadLink)

export const client = new ApolloClient({
    link: splitLink,
    cache: new InMemoryCache({
        typePolicies: {
            Query: {
                fields: {
                    messages: offsetLimitPagination(['conversationId']),
                },
            },
        },
    }),
})

const App: FC<AppProps> = ({ Component, pageProps }) => {
    return (
        <ThemeProvider theme={theme}>
            <ApolloProvider client={client}>
                <Layout>
                    <Component {...pageProps} />
                </Layout>
            </ApolloProvider>
        </ThemeProvider>
    )
}

export default App
