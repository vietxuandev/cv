import React, { FC, memo, useEffect, useState } from 'react'
import Link from 'next/link'
import {
    Box,
    Button,
    Field,
    Flex,
    Message,
    Text,
    Checkbox,
    Label,
} from 'theme-ui'
import { useForm, SubmitHandler } from 'react-hook-form'
import { useMutation } from '@apollo/client'
import { useRouter } from 'next/router'
import useLocalStorage from '../hooks/local-storage'
import { SIGN_UP } from '../gql'

interface IFormInput {
    email: string
    password: string
    firstName: string
    lastName: string
    phoneNumber: string
}

const SignUpPage: FC = () => {
    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm<IFormInput>()
    const [signUp, { data, loading, error }] = useMutation<
        { signUp: string } | undefined
    >(SIGN_UP, {
        errorPolicy: 'all',
        fetchPolicy: 'no-cache',
    })
    const onSubmit: SubmitHandler<IFormInput> = (params) =>
        signUp({ variables: params })
    const {
        storedValue: authToken,
        setValue: setAuthToken,
    } = useLocalStorage<string>('auth-token', '')
    const router = useRouter()
    const [checked, setChecked] = useState<boolean>(false)
    useEffect(() => {
        if (data?.signUp) {
            setAuthToken(data.signUp)
        }
    }, [data?.signUp, setAuthToken])
    useEffect(() => {
        if (authToken) {
            router.push('/')
        }
    }, [authToken, router])
    return (
        <>
            <Flex
                as="form"
                onSubmit={handleSubmit(onSubmit)}
                sx={{ flexDirection: 'column' }}
            >
                <Box mb={3}>
                    <Field
                        label="Tên"
                        {...register('firstName', {
                            required: 'Vui lòng nhập Tên',
                        })}
                    />
                    {errors.firstName && (
                        <Text color="red" sx={{ fontSize: 1 }}>
                            {errors.firstName.message}
                        </Text>
                    )}
                </Box>
                <Box mb={3}>
                    <Field
                        label="Họ"
                        {...register('lastName', {
                            required: 'Vui lòng nhập Họ',
                        })}
                    />
                    {errors.lastName && (
                        <Text color="red" sx={{ fontSize: 1 }}>
                            {errors.lastName.message}
                        </Text>
                    )}
                </Box>
                <Box mb={3}>
                    <Field
                        label="Email"
                        {...register('email', {
                            required: 'Vui lòng nhập Email',
                            pattern: {
                                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                                message: 'Email không hợp lệ',
                            },
                        })}
                    />
                    {errors.email && (
                        <Text color="red" sx={{ fontSize: 1 }}>
                            {errors.email.message}
                        </Text>
                    )}
                </Box>
                <Box mb={3}>
                    <Field
                        label="Số điện thoại"
                        {...register('phoneNumber', {
                            required: 'Vui lòng nhập Số điện thoại',
                        })}
                    />
                    {errors.phoneNumber && (
                        <Text color="red" sx={{ fontSize: 1 }}>
                            {errors.phoneNumber.message}
                        </Text>
                    )}
                </Box>
                <Box mb={3}>
                    <Field
                        label="Mật khẩu"
                        type="password"
                        {...register('password', {
                            required: 'Vui lòng nhập Mật khẩu',
                        })}
                    />
                    {errors.password && (
                        <Text color="red" sx={{ fontSize: 1 }}>
                            {errors.password.message}
                        </Text>
                    )}
                </Box>
                {error && (
                    <Message variant="error" mb={3}>
                        {error.graphQLErrors
                            .map(({ message }) => message)
                            .join(', ')}
                    </Message>
                )}
                <Flex>
                    <Label sx={{ display: 'flex', alignItems: 'center' }}>
                        <Checkbox
                            checked={checked}
                            onChange={() => setChecked(!checked)}
                        />
                        <Text sx={{ fontSize: 2 }}>
                            Đồng ý với{' '}
                            <Link href="/" passHref>
                                <Text
                                    as="a"
                                    sx={{
                                        fontSize: 2,
                                        textDecoration: 'underline',
                                    }}
                                >
                                    điều khoản
                                </Text>
                            </Link>{' '}
                            của chúng tôi
                        </Text>
                    </Label>
                    <Flex sx={{ flexShrink: 0 }}>
                        <Button
                            mr={2}
                            disabled={loading || !checked}
                            sx={{
                                cursor: checked ? 'pointer' : 'not-allowed',
                            }}
                        >
                            Đăng ký
                        </Button>
                        <Link href="/login" passHref>
                            <Button as="a" variant="secondary">
                                Đăng nhập
                            </Button>
                        </Link>
                    </Flex>
                </Flex>
            </Flex>
        </>
    )
}

export default memo(SignUpPage)
